# DYNAMODB_PING for JGroups

JGroups discovery protocol using a shared Amazon Web Services (JWS) DynamoDB 
table.


## Configuration

* **region_name** - The AWS region name

* **endpoint** - The HTTP(S) endpoint of DynamoDB. If set overrides the 
  `region_name` setting. Use this setting for testing DYNAMODB_PING with a 
  local DynamoDB instance.
  
* **table_name** - The DynamoDB table name to use, defaults to `jgroups_ping`.

* **check_if_table_exists** - If `true` a check will be made at startup if the 
  DynamoDB table exists, and if not, will create one. Defaults to `true`.
  
* **cluster_name_prefix** - Optional prefix to be applied to the cluster name 
  when stored in the DynamoDB ping table. Allows multiple clusters with the 
  same name to share the same DynamoDB table.
  
* **store_debug_info** - If `true` will cause additional human readable 
  debugging information to be stored in each ping DynamoDB item. Defaults to 
  `false`.

Example configuration XML snippet, the AWS region can be set via the 
`aws.region` Java system property, defaults to `us-east-1`:

```xml
<DYNAMODB_PING region_name="${aws.region:us-east-1}"
               store_debug_info="true"/>
```


## AWS credentials
 
Make sure the AWS credentials for accessing the DynamoDB ping table are 
configured in way that the default AWS credentials provider chain can look them 
up, e.g. by setting the `aws.accessKeyId` and `aws.secretKey` Java 
system properties. See
http://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html


## Compatibility

JGroups v3.6+


## Maven coordinates

```xml
<dependency>
    <groupId>com.nimbusds</groupId>
    <artifactId>jgroups-dynamodb-ping</artifactId>
    <version>[ version ]</version>
</dependency>
```
where *version* should be the latest stable version.


## Git repo

https://bitbucket.org/connect2id/jgroups-dynamodb-ping


## License

Apache 2.0


## Change log

* version 1.0 (2017-10-23)
    * First release.

* version 1.0.1 (2017-10-23)
    * Fixes DynamoDB scan spec composition.

* version 1.0.2 (2017-10-23)
    * Removes redundant log statement prefix.

* version 1.1 (2017-10-23)
    * Adds "timestamp" in ISO date/time format when "store_debug_info" is set
      to true.

* version 1.2 (2017-10-24)
    * Adds optional "cluster_name_prefix" configuration property.

* version 1.2.1 (2017-11-02)
    * Checks that "endpoint" is not empty or blank before using it as the 
      preferred DynamoDB table coordinate to "region_name".

* version 1.2.2 (2017-11-24)
    * Switches to com.amazonaws:aws-java-sdk-bundle:1.11.235 dependency with 
      shaded transient dependencies to prevent potential conflicts.

* version 1.2.3 (2019-12-31)
    * Updates to com.amazonaws:aws-java-sdk-bundle:1.11.632
    
* version 1.2.4 (2020-06-22)
    * Fixes JGroups to org.jgroups:jgroups:4.0.21.Final
    * Updates to com.amazonaws:aws-java-sdk-bundle:1.11.782

* version 1.2.5 (2021-02-16)
    * Replaces com.amazonaws:aws-java-sdk-bundle dependency with 
      com.amazonaws:aws-java-sdk-dynamodb:1.11.955
    * Updates to org.jgroups:jgroups:4.0.24.Final
     
* version 1.2.6 (2023-02-27)
    * Updates to com.amazonaws:aws-java-sdk-dynamodb:1.12.415
    * Updates to org.jgroups:jgroups:5.2.10.Final
