package org.jgroups.protocols;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import org.jgroups.util.Responses;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


// Expects local DynamoDB started with java -jar  opt/dynamodb-local/DynamoDBLocal.jar -inMemory -sharedDb
public class DYNAMODB_PING_TestWithBackend {
	
	
	static final String ENDPOINT = "http://localhost:8000";
	
	
	static final String ACCESS_KEY_ID = "test";
	
	
	static final String SECRET_KEY = "test";
	
	
	AmazonDynamoDB client;
	
	
	@Before
	public void setUp() {
		
		client = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_KEY)))
			.withEndpointConfiguration(
				new AwsClientBuilder.EndpointConfiguration(
					ENDPOINT,
					null // inferred from endpoint
				))
			.build();
		
		for (String tableName: client.listTables().getTableNames()) {
			
			try {
				Table table = new DynamoDB(client).getTable(tableName);
				table.delete();
				table.waitForDelete();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	
	@After
	public void tearDown() {
	
		if (client != null)
			client.shutdown();
	}
	
	
	@Test
	public void testCreateTableIfMissing()
		throws Exception {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		
		ping.createTableIfMissing(client);
		
		Table table = new DynamoDB(client).getTable(ping.table_name);
		
		table.waitForActive();
		
		TableDescription tableDescription = table.describe();
		assertEquals("jgroups_ping", tableDescription.getTableName());
		assertEquals(0L, tableDescription.getItemCount().longValue());
		assertEquals(1L, tableDescription.getProvisionedThroughput().getReadCapacityUnits().longValue());
		assertEquals(1L, tableDescription.getProvisionedThroughput().getWriteCapacityUnits().longValue());
	}
	
	
	@Test
	public void testReadAll()
		throws Exception {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		
		ping.createTableIfMissing(client);
		
		ping.table = new DynamoDB(client).getTable(ping.table_name);
		
		Responses responses = new Responses(false);
		
		ping.readAll(null, "my-cluster", responses);
		
		assertTrue(responses.isEmpty());
	}
}
