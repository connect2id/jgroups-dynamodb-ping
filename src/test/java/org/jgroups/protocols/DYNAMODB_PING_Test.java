package org.jgroups.protocols;


import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.model.*;
import junit.framework.TestCase;
import org.jgroups.conf.ClassConfigurator;
import org.junit.Test;


public class DYNAMODB_PING_Test {
	
	@Test
	public void testProtocolMagicNumber() {
		
		assertTrue(DYNAMODB_PING.JGROUPS_PROTOCOL_MAGIC_NUMBER > 1024);
		assertEquals(1050, DYNAMODB_PING.JGROUPS_PROTOCOL_MAGIC_NUMBER);
	}
	
	
	@Test
	public void testRegisterProtocolWithJGroups() {
		new DYNAMODB_PING();
		assertEquals(1050, ClassConfigurator.getProtocolId(DYNAMODB_PING.class));
		assertEquals(DYNAMODB_PING.class, ClassConfigurator.getProtocol(DYNAMODB_PING.JGROUPS_PROTOCOL_MAGIC_NUMBER));
	}
	
	
	@Test
	public void testDefaultProperties() {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		assertNull(ping.region_name);
		assertNull(ping.endpoint);
		assertEquals("jgroups_ping", ping.table_name);
		assertTrue(ping.check_if_table_exists);
		assertEquals("", ping.cluster_name_prefix);
		assertFalse(ping.store_debug_info);
	}
	
	
	@Test
	public void testComposeClusterName() {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		assertEquals("ISPN", ping.composeStoredClusterName("ISPN"));
		
		ping.cluster_name_prefix = null;
		assertEquals("ISPN", ping.composeStoredClusterName("ISPN"));
		
		ping.cluster_name_prefix = "tid-123-";
		assertEquals("tid-123-ISPN", ping.composeStoredClusterName("ISPN"));
	}
	
	
	@Test
	public void testComposeCreateTableRequest() {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		ping.table_name = "dynamodb_ping";
		
		CreateTableRequest createTableRequest = ping.composeCreateTableRequest();
		
		assertEquals("dynamodb_ping", createTableRequest.getTableName());
		
		List<KeySchemaElement> keySchemaElements = createTableRequest.getKeySchema();
		assertEquals("own_address", keySchemaElements.get(0).getAttributeName());
		assertEquals(KeyType.HASH.toString(), keySchemaElements.get(0).getKeyType());
		assertEquals("cluster", keySchemaElements.get(1).getAttributeName());
		assertEquals(KeyType.RANGE.toString(), keySchemaElements.get(1).getKeyType());
		assertEquals(2, keySchemaElements.size());
		
		List<AttributeDefinition> attributeDefinitions = createTableRequest.getAttributeDefinitions();
		assertEquals("own_address", attributeDefinitions.get(0).getAttributeName());
		assertEquals("S", attributeDefinitions.get(0).getAttributeType());
		assertEquals("cluster", attributeDefinitions.get(1).getAttributeName());
		assertEquals("S", attributeDefinitions.get(1).getAttributeType());
		assertEquals(2, attributeDefinitions.size());
		
		ProvisionedThroughput provisionedThroughput = createTableRequest.getProvisionedThroughput();
		assertEquals(1L, provisionedThroughput.getReadCapacityUnits().longValue());
		assertEquals(1L, provisionedThroughput.getWriteCapacityUnits().longValue());
	}
	
	
	@Test
	public void testComposeScanSpec() {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		ping.table_name = "dynamodb_ping";
		
		ScanSpec scanSpec = ping.composeScanSpec("my-cluster");
		
		assertEquals("#k_cluster = :v_cluster", scanSpec.getFilterExpression());
		
		assertTrue(scanSpec.getNameMap().containsKey("#k_cluster"));
		assertTrue(scanSpec.getNameMap().containsValue("cluster"));
		assertEquals(1, scanSpec.getNameMap().size());
		
		assertTrue(scanSpec.getValueMap().containsKey(":v_cluster"));
		assertTrue(scanSpec.getValueMap().containsValue("my-cluster"));
		assertEquals(1, scanSpec.getValueMap().size());
		
		assertNull(scanSpec.getProjectionExpression());
	}
	
	
	@Test
	public void testComposeScanSpec_withClusterNamePrefix() {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		ping.table_name = "dynamodb_ping";
		ping.cluster_name_prefix = "tid-123-";
		
		ScanSpec scanSpec = ping.composeScanSpec("my-cluster");
		
		assertEquals("#k_cluster = :v_cluster", scanSpec.getFilterExpression());
		
		assertTrue(scanSpec.getNameMap().containsKey("#k_cluster"));
		assertTrue(scanSpec.getNameMap().containsValue("cluster"));
		assertEquals(1, scanSpec.getNameMap().size());
		
		assertTrue(scanSpec.getValueMap().containsKey(":v_cluster"));
		assertTrue(scanSpec.getValueMap().containsValue("tid-123-my-cluster"));
		assertEquals(1, scanSpec.getValueMap().size());
		
		assertNull(scanSpec.getProjectionExpression());
	}
	
	
	@Test
	public void testComposePrimaryKey() {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		
		PrimaryKey primaryKey = ping.composePrimaryKey("192.168.0.1", "my-cluster");
		
		Collection<KeyAttribute> keyAttributes = primaryKey.getComponents();
		
		for (KeyAttribute a: keyAttributes) {
			
			switch (a.getName()) {
				
				case "own_address":
					assertEquals("192.168.0.1", a.getValue());
					break;
					
				case "cluster":
					assertEquals("my-cluster", a.getValue());
					break;
					
				default:
					fail();
			}
		}
		
		assertEquals(2, keyAttributes.size());
	}
	
	
	@Test
	public void testComposePrimaryKey_withClusterNamePrefix() {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		ping.cluster_name_prefix = "tid-123-";
		
		PrimaryKey primaryKey = ping.composePrimaryKey("192.168.0.1", "my-cluster");
		
		Collection<KeyAttribute> keyAttributes = primaryKey.getComponents();
		
		for (KeyAttribute a: keyAttributes) {
			
			switch (a.getName()) {
				
				case "own_address":
					assertEquals("192.168.0.1", a.getValue());
					break;
					
				case "cluster":
					assertEquals("tid-123-my-cluster", a.getValue());
					break;
					
				default:
					fail();
			}
		}
		
		assertEquals(2, keyAttributes.size());
	}
	
	
	@Test
	public void testConvertItemToPingDataAndBack()
		throws Exception {
		
		String itemJSON = "{" +
			"  \"physical_address\" : \"192.168.0.104:7800\"," +
			"  \"cluster\" : \"ISPN\"," +
			"  \"ping_data\" : \"An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w==\"," +
			"  \"server\" : true," +
			"  \"logical_name\" : \"dev-28768\"," +
			"  \"coordinator\" : true," +
			"  \"own_address\" : \"a0249362-3cf3-07f2-7ea6-cd8910ef7b14\"" +
			"}";
		
		Item item = Item.fromJSON(itemJSON);
		
		// ping_data must be stored as raw binary data
		byte[] pingDataRaw = Base64.getDecoder().decode("An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w==");
		item.withBinary("ping_data", pingDataRaw);
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		
		PingData pingData = ping.toPingData(item);
		
		assertEquals("a0249362-3cf3-07f2-7ea6-cd8910ef7b14", DYNAMODB_PING.addressAsString(pingData.getAddress()));
		assertEquals("dev-28768", pingData.getLogicalName());
		assertEquals("192.168.0.104:7800", pingData.getPhysicalAddr().toString());
		assertTrue(pingData.isServer());
		assertTrue(pingData.isCoord());
		assertNull(pingData.mbrs());
		
		// Some parameters are lost during round-trip
		item = ping.toItem(pingData, "ISPN");
		assertEquals("ISPN", item.getString("cluster"));
		assertEquals("a0249362-3cf3-07f2-7ea6-cd8910ef7b14", item.getString("own_address"));
		assertEquals("An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w==", Base64.getEncoder().encodeToString(item.getBinary("ping_data")));
		assertEquals(3, item.numberOfAttributes());
	}
	
	
	@Test
	public void testConvertItemToPingDataAndBack_alternative()
		throws Exception {
		
		String itemJSON = "{\n" +
			"  \"physical_address\" : \"192.168.0.105:7800\",\n" +
			"  \"cluster\" : \"ISPN\",\n" +
			"  \"ping_data\" : \"AqwxXbiTsFUj5mQrUQYTlCoBAQALZnJlc2gtMTIxMDgQBMCoAGkeeP//\",\n" +
			"  \"server\" : true,\n" +
			"  \"logical_name\" : \"fresh-12108\",\n" +
			"  \"coordinator\" : false,\n" +
			"  \"own_address\" : \"e6642b51-0613-942a-ac31-5db893b05523\"\n" +
			"}";
		
		Item item = Item.fromJSON(itemJSON);
		
		// ping_data must be stored as raw binary data
		byte[] pingDataRaw = Base64.getDecoder().decode("AqwxXbiTsFUj5mQrUQYTlCoBAQALZnJlc2gtMTIxMDgQBMCoAGkeeP//");
		item.withBinary("ping_data", pingDataRaw);
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		ping.store_debug_info = true;
		
		PingData pingData = ping.toPingData(item);
		
		assertEquals("e6642b51-0613-942a-ac31-5db893b05523", DYNAMODB_PING.addressAsString(pingData.getAddress()));
		assertEquals("fresh-12108", pingData.getLogicalName());
		assertEquals("192.168.0.105:7800", pingData.getPhysicalAddr().toString());
		assertTrue(pingData.isServer());
		assertFalse(pingData.isCoord());
		assertNull(pingData.mbrs());
		
		item = ping.toItem(pingData, "ISPN");
		assertEquals("ISPN", item.getString("cluster"));
		assertEquals("e6642b51-0613-942a-ac31-5db893b05523", item.getString("own_address"));
		assertEquals("fresh-12108", item.getString("logical_name"));
		assertEquals("192.168.0.105:7800", item.getString("physical_address"));
		assertTrue(item.getBoolean("server"));
		assertFalse(item.getBoolean("coordinator"));
		assertNotNull(item.getString("timestamp"));
		assertEquals("AqwxXbiTsFUj5mQrUQYTlCoBAQALZnJlc2gtMTIxMDgQBMCoAGkeeP//", Base64.getEncoder().encodeToString(item.getBinary("ping_data")));
		assertEquals(8, item.numberOfAttributes());
	}
	
	
	@Test
	public void testFormatISODate() {
		
		assertEquals("2017-10-23 21:41:54", DYNAMODB_PING.formatISODate(new Date(1508784114282L)));
	}
	
	
	@Test
	public void testToItem()
		throws Exception {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		
		PingData pingData = ping.deserialize(Base64.getDecoder().decode("An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w=="));
		
		Item item = ping.toItem(pingData, "ISPN");
		assertEquals("a0249362-3cf3-07f2-7ea6-cd8910ef7b14", item.getString("own_address"));
		assertEquals("ISPN", item.getString("cluster"));
		assertEquals("An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w==", Base64.getEncoder().encodeToString(item.getBinary("ping_data")));
		assertEquals(3, item.numberOfAttributes());
	}
	
	
	@Test
	public void testToItem_withClusterNamePrefix()
		throws Exception {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		ping.cluster_name_prefix = "tid-123-";
		
		PingData pingData = ping.deserialize(Base64.getDecoder().decode("An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w=="));
		
		Item item = ping.toItem(pingData, "ISPN");
		assertEquals("a0249362-3cf3-07f2-7ea6-cd8910ef7b14", item.getString("own_address"));
		assertEquals("tid-123-ISPN", item.getString("cluster"));
		assertEquals("An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w==", Base64.getEncoder().encodeToString(item.getBinary("ping_data")));
		assertEquals(3, item.numberOfAttributes());
	}
	
	
	@Test
	public void testToItem_withStoreDebugInfo()
		throws Exception {
		
		DYNAMODB_PING ping = new DYNAMODB_PING();
		ping.store_debug_info = true;
		
		PingData pingData = ping.deserialize(Base64.getDecoder().decode("An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w=="));
		
		Item item = ping.toItem(pingData, "ISPN");
		assertEquals("a0249362-3cf3-07f2-7ea6-cd8910ef7b14", item.getString("own_address"));
		assertEquals("ISPN", item.getString("cluster"));
		assertEquals("An6mzYkQ73sUoCSTYjzzB/IDAQAJZGV2LTI4NzY4EATAqABoHnj//w==", Base64.getEncoder().encodeToString(item.getBinary("ping_data")));
		assertEquals("dev-28768", item.getString("logical_name"));
		assertEquals("192.168.0.104:7800", item.getString("physical_address"));
		assertTrue(item.getBoolean("server"));
		assertTrue(item.getBoolean("coordinator"));
		assertNotNull(item.getString("timestamp"));
		assertEquals(8, item.numberOfAttributes());
	}
}
