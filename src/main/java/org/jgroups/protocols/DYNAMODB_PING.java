package org.jgroups.protocols;


import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.*;
import org.jgroups.Address;
import org.jgroups.annotations.Property;
import org.jgroups.conf.ClassConfigurator;
import org.jgroups.util.Responses;


/**
 * <p>JGroups discovery protocol using a shared Amazon Web Services (JWS)
 * DynamoDB table.</p>
 *
 * @author Vladimir Dzhuvinov
 */
public class DYNAMODB_PING extends FILE_PING {
	
	
	/**
	 * The DYNAMODB_PING magic number.
	 */
	protected static final short JGROUPS_PROTOCOL_MAGIC_NUMBER = 1050;
	
	
	@Property(description="The DynamoDB region to use.", exposeAsManagedAttribute=false)
	protected String region_name;
	
	
	@Property(description="The DynamoDB endpoint to use (optional, overrides region).", exposeAsManagedAttribute=false)
	protected String endpoint;
	
	
	@Property(description="The DynamoDB table to use (defaults to 'jgroups_ping').", exposeAsManagedAttribute=false)
	protected String table_name = "jgroups_ping";
	
	
	@Property(description="Checks if the DynamoDB table exists and creates a new one if missing.", exposeAsManagedAttribute=false)
	protected boolean check_if_table_exists = true;
	
	
	@Property(description="Optional prefix to be applied to the cluster name when stored in the DynamoDB ping table.", exposeAsManagedAttribute=false)
	protected String cluster_name_prefix = "";
	
	
	@Property(description="Causes additional debugging information to be stored for each DynamoDB ping item.", exposeAsManagedAttribute=false)
	protected boolean store_debug_info = false;
	
	
	/**
	 * The DynamoDB ping table.
	 */
	protected Table table;
	
	
	static {
		registerProtocolWithJGroups(JGROUPS_PROTOCOL_MAGIC_NUMBER);
	}
	
	
	/**
	 * Registers the DYNAMODB_PING protocol with JGroups.
	 *
	 * @param magicNumber The magic number to use. Should be greater than
	 *                    1024.
	 */
	private static void registerProtocolWithJGroups(final short magicNumber) {
		
		if (ClassConfigurator.getProtocolId(DYNAMODB_PING.class) == 0) {
			ClassConfigurator.addProtocol(magicNumber, DYNAMODB_PING.class);
		}
	}
	
	
	@Override
	public void init() throws Exception {
		
		super.init();
		
		AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(DefaultAWSCredentialsProviderChain.getInstance());
		
		if (endpoint != null && ! endpoint.trim().isEmpty()) {
			// Override region if the endpoint is set
			builder = builder.withEndpointConfiguration(
				new AwsClientBuilder.EndpointConfiguration(
					endpoint,
					null // inferred from endpoint
				));
			log.info("set DynamoDB endpoint to %s", endpoint);
		} else {
			builder.withRegion(region_name);
		}
		
		AmazonDynamoDB client = builder.build();
		
		log.info("using DynamoDB in region %s with table %s", region_name, table_name);
		
		if (check_if_table_exists) {
			createTableIfMissing(client);
		}
		
		table = new DynamoDB(client).getTable(table_name);
		
		try {
			table.waitForActive();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * Creates a new DynamoDB ping table if missing.
	 *
	 * @param client The DynamoDB client.
	 */
	protected void createTableIfMissing(final AmazonDynamoDB client) {
		
		try {
			new DynamoDB(client).createTable(composeCreateTableRequest());
			log.info("created DynamoDB table %s", table_name);
		} catch (ResourceInUseException e) {
			log.info("found DynamoDB table %s", table_name);
		}
	}
	
	
	/**
	 * Composes a create table request for the DynamoDB ping table.
	 *
	 * @return The create table request.
	 */
	protected CreateTableRequest composeCreateTableRequest() {
		
		Collection<KeySchemaElement> keyAttributes = new LinkedList<>();
		keyAttributes.add(new KeySchemaElement("own_address", KeyType.HASH));
		keyAttributes.add(new KeySchemaElement("cluster", KeyType.RANGE));
		
		Collection<AttributeDefinition> attributeDefinitions = new LinkedList<>();
		attributeDefinitions.add(new AttributeDefinition("own_address", ScalarAttributeType.S));
		attributeDefinitions.add(new AttributeDefinition("cluster", ScalarAttributeType.S));
		
		return new CreateTableRequest()
			.withTableName(table_name)
			.withKeySchema(keyAttributes)
			.withAttributeDefinitions(attributeDefinitions)
			.withProvisionedThroughput(new ProvisionedThroughput(1L, 1L));
	}
	
	
	@Override
	public void stop() {
		
		super.stop();
		
		if (is_coord)
			removeAll(cluster_name);
	}
	
	
	@Override
	protected void createRootDir() {
		// ignore, table has to exist
	}
	
	
	@Override
	protected void readAll(final List<Address> members, final String clusterName, final Responses responses) {
		
		if (clusterName == null)
			return;
		
		if (log.isTraceEnabled())
			log.trace("getting entries for cluster %s ...", clusterName);
		
		ItemCollection<ScanOutcome> outcome;
		
		try {
			outcome = table.scan(composeScanSpec(clusterName));
		} catch (Exception e) {
			log.error(String.format("failed to get member list for cluster %s", clusterName), e);
			return;
		}
			
		if (log.isTraceEnabled())
			log.trace("retrieved %d items for cluster %s", outcome.getAccumulatedItemCount(), clusterName);
		
		for (Item item : outcome) {
			try {
				final PingData pingData = toPingData(item);
				
				if (log.isTraceEnabled())
					log.trace("processing DynamoDB item [%s -> %s]", item.getString("own_address"), pingData);
				
				if (pingData == null || (members != null && ! members.contains(pingData.getAddress())))
					continue;
				
				responses.addResponse(pingData, pingData.isCoord());
				
				if (log.isTraceEnabled())
					log.trace("added member %s [members: %s]", pingData, members);
				
				if (local_addr != null && ! local_addr.equals(pingData.getAddress())) {
					
					addDiscoveryResponseToCaches(
						pingData.getAddress(),
						pingData.getLogicalName(),
						pingData.getPhysicalAddr());
					
					if (log.isTraceEnabled())
						log.trace("added possible member %s [local address: %s]", pingData, local_addr);
				}
				
			} catch (Exception e) {
				log.error("error processing ping data for cluster %s [item: %s]", clusterName, item);
			}
		}
	}
	
	
	/**
	 * Composes the final stored cluster name.
	 *
	 * @return The cluster name.
	 */
	protected String composeStoredClusterName(final String clusterName) {
		
		return cluster_name_prefix != null ? cluster_name_prefix + clusterName : clusterName;
	}
	
	
	/**
	 * Composes the DynamoDB primary key for the specified JGroups address
	 * an cluster name.
	 *
	 * @param ownAddress  The own address.
	 * @param clusterName The cluster name.
	 *
	 * @return The primary key.
	 */
	protected PrimaryKey composePrimaryKey(final String ownAddress, final String clusterName) {
		
		return new PrimaryKey(
			"own_address", ownAddress, // hash key
			"cluster", composeStoredClusterName(clusterName) // range key
		);
	}
	
	
	/**
	 * Composes the DynamoDB scan spec for the specified cluster name.
	 *
	 * @param clusterName The cluster name.
	 *
	 * @return The scan spec.
	 */
	protected ScanSpec composeScanSpec(final String clusterName) {
		
		return new ScanSpec()
			.withFilterExpression("#k_cluster = :v_cluster")
			.withNameMap(new NameMap().with("#k_cluster", "cluster"))
			.withValueMap(new ValueMap().withString(":v_cluster", composeStoredClusterName(clusterName)));
	}
	
	
	/**
	 * Formats the specified date in ISO format.
	 *
	 * @param date The date.
	 *
	 * @return The formatted date.
	 */
	static String formatISODate(final Date date) {
	
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}
	
	
	/**
	 * Converts the specified ping data to a DynamoDB item.
	 *
	 * @param data        The ping data.
	 * @param clusterName The cluster mame.
	 *
	 * @return The DynamoDB item.
	 */
	protected Item toItem(final PingData data, final String clusterName) {
		
		Item item = new Item()
			.withPrimaryKey(composePrimaryKey(addressAsString(data.getAddress()), clusterName))
			.withBinary("ping_data", serializeWithoutView(data));
		
		if (store_debug_info) {
			item
				.withString("logical_name", data.getLogicalName())
				.withString("physical_address", addressAsString(data.getPhysicalAddr()))
				.withBoolean("server", data.isServer())
				.withBoolean("coordinator", data.isCoord())
				.withString("timestamp", formatISODate(new Date()));
		}
		
		return item;
	}
	
	
	/**
	 * Converts the specified DynamoDB item to ping data.
	 *
	 * @param item The DynamoDB item.
	 *
	 * @return The ping data.
	 *
	 * @throws Exception If conversion failed.
	 */
	protected PingData toPingData(final Item item)
		throws Exception {
	
		return deserialize(item.getBinary("ping_data"));
	}
	
	
	@Override
	protected void write(final List<PingData> list, final String clusterName) {
		
		for (PingData data: list) {
			putIntoTable(data, clusterName);
		}
	}
	
	
	/**
	 * Puts the specified ping data into the DynamoDB table.
	 *
	 * @param data        The ping data.
	 * @param clusterName The cluster name.
	 */
	protected synchronized void putIntoTable(final PingData data, final String clusterName) {
		
		try {
			table.putItem(toItem(data, clusterName));
		} catch (Exception e) {
			log.error("put error: " + e.getMessage(), e);
		}
	}
	
	
	@Override
	protected void remove(final String clusterName, final Address address) {
		try {
			table.deleteItem(composePrimaryKey(addressAsString(address), clusterName));
		} catch (Exception e) {
			log.error("delete error: " + e.getMessage(), e);
		}
	}
	
	
	@Override
	protected void removeAll(final String clusterName) {
		
		if (clusterName == null)
			return;
		
		ItemCollection<ScanOutcome> outcome = table.scan(composeScanSpec(clusterName));
		
		for (Item item: outcome) {
			try {
				remove(clusterName, toPingData(item).getAddress());
			} catch (Exception e) {
				log.error("delete all error: " + e.getMessage(), e);
			}
		}
	}
}
